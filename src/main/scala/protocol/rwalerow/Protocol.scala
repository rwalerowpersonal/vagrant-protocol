package protocol.rwalerow

case object PingMessage
case object PongMessage
case object StartMessage
case object StopMessage
